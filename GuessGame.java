
public class GuessGame {
    Player player1;
    Player player2;
    Player player3;

    /**
     * в данном класе реализована игра по угадыванию чисел,
     * в качестве играков используется  рандом
     * в начале игры выбирается число и наши вертуальные играки пытаются его угодать
     * если один из игроков угадывает число игра заканчивается если же нет им даются другие возможности угадать это число
     */
    public void startGame() {
        player1 = new Player();
        player2 = new Player();
        player3 = new Player();

        byte playerNumber1 = 0;
        byte playerNumber2 = 0;
        byte playerNumber3 = 0;

        boolean player1Won = false;
        boolean player2Won = false;
        boolean player3Won = false;

        byte guessTheNumber = (byte) (Math.random() * 10);
        System.out.println("Я думаю о число между 0 и 9...");

        while (true) {
            System.out.println("Число, которое нужно угадать " + guessTheNumber);

            player1.quess();
            player2.quess();
            player3.quess();

            playerNumber1 = (byte) player1.number;
            System.out.println("Игрок один угадывает число " + playerNumber1);
            playerNumber2 = (byte) player2.number;
            System.out.println("Игрок два угадывает число " + playerNumber2);
            playerNumber3 = (byte) player3.number;
            System.out.println("Игрок три угадывает число " + playerNumber3);


            if (playerNumber1 == guessTheNumber) {
                player1Won = true;
            }
            if (playerNumber2 == guessTheNumber) {
                player2Won = true;
            }
            if (playerNumber3 == guessTheNumber) {
                player3Won = true;
            }
            if (player1Won || player2Won || player3Won) {
                System.out.println("У нас есть победитель!" + "\n" +
                        "Игрок один угадал? " + player1Won + "\n" +
                        "Игрок два угадал? " + player2Won + "\n" +
                        "Игрок три угадал? " + player3Won + "\n" +
                        "Конец игры");
                break;
            } else {
                System.out.println("Игрокам дают возможность попробовать еще раз.");
            }
        }
    }
}